﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleScript : MonoBehaviour
{
    public float grappleRange = 10f;
    public LayerMask canGrapple;
    public LineRenderer line;
    public float step = .05f;

    DistanceJoint2D joint;
    Vector3 targetPos;
    RaycastHit2D hit;

    // Start is called before the first frame update
    void Start()
    {
        joint = GetComponent<DistanceJoint2D>();
        joint.enabled = false;
        line.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(joint.distance > 0.5)
        {
            joint.distance -= step;
        }
        if (Input.GetKeyDown("mouse 0"))
        {
            targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetPos.z = 0;

            hit = Physics2D.Raycast(transform.position, targetPos - transform.position, grappleRange, canGrapple);

            if (hit.collider != null && hit.collider.gameObject.GetComponent<Rigidbody2D>() != null)
            {
                joint.enabled = true;
                joint.connectedBody = hit.collider.gameObject.GetComponent<Rigidbody2D>();
                joint.connectedAnchor = hit.point - new Vector2(hit.collider.transform.position.x, hit.collider.transform.position.y);
                joint.distance = Vector2.Distance(transform.position, hit.point);

                line.enabled = true;
                line.SetPosition(0, transform.position);
                line.SetPosition(1, hit.point);
            }
        }
        if(Input.GetKey("mouse 0"))
        {
            line.SetPosition(0, transform.position);
        }
        if (Input.GetKeyUp("mouse 0"))
        {
            joint.enabled = false;
            line.enabled = false;
        }
    }
}
