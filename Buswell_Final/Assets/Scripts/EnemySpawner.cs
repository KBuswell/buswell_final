﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public float initialDelay;
    public float minDelay;
    public GameObject enemy;

    float delay;
    float timer;
    int count = 5;
    // Start is called before the first frame update
    void Start()
    {
        delay = initialDelay;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= delay)
        {
            Instantiate(enemy, transform.position, transform.rotation);
            timer = 0;
            count--;
            if(count == 0 && delay > minDelay)
            {
                delay -= 0.5f;
                count = 5;
            }
        }
    }
}
