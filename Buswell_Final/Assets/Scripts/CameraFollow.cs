﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;

    public float smoothing =2f;

    // Update is called once per frame
    void LateUpdate()
    {
        if (player != null)
        {
            Vector3 targetPos = player.transform.position;
            targetPos.z = -30;
            transform.position = Vector3.Lerp(transform.position, targetPos, smoothing * Time.deltaTime);
        }
    }
}
