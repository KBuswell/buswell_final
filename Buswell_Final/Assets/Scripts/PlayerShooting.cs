﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.6f;
    public float range = 100f;
    public Bullet bullet;

    public Vector3 worldPos;
    float timer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    Rigidbody2D playerRigidBody;
    int shootableMask;
    int edgeMask;
    float camRayLength = 10000f;


    void Awake()
    {
        shootableMask = LayerMask.GetMask("Shootable");
        edgeMask = LayerMask.GetMask("Edge");
        playerRigidBody = GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        timer += Time.deltaTime;
        Turning();

        if (Input.GetButton("Fire2") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            Shoot();
            timer = 0;
        }

    }

    void Turning()
    {

        Vector2 mousePos = Input.mousePosition;

        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        worldPos = new Vector3(mousePos.x, mousePos.y, 0f);

        Vector3 targetDir = worldPos - transform.position;

        transform.rotation = Quaternion.LookRotation(targetDir, Vector2.up);
    }

    void Shoot()
    {
        Vector3 currentRotation = transform.rotation.eulerAngles;
        
        Instantiate(bullet, transform.position, transform.rotation);
       //spawn bullet with the same rotation as the shoot point
    }
}
