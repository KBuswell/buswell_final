﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
   Transform player;
    public float speed = 400f;
    public float jumpMin = .25f;
    public float jumpMax = 3f;
    public float jumpForce = 800f;

    float jumpTimer;
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        jumpTimer = Random.Range(jumpMin, jumpMax);
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }
     
    void FixedUpdate()
    {
        if (player != null)
        {
            if (player.position.x > transform.position.x)
            {
                rb.AddForce(Vector2.right * speed);
            }
            else if (player.position.x < transform.position.x)
            {
                rb.AddForce(Vector2.right * -speed);
            }
        }

        jumpTimer -= Time.deltaTime;
 
        if (jumpTimer <= 0)
        {
            rb.AddForce(new Vector2(0f, jumpForce + Random.Range(-100,100)));
            jumpTimer = Random.Range(jumpMin, jumpMax);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "PlayerBullet")
        {
            Destroy(gameObject);
            Destroy(collision.gameObject);
            GameController.gc.score++;
        }
    }
}
